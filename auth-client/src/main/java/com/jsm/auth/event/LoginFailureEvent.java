package com.jsm.auth.event;

import java.util.UUID;
import org.springframework.cloud.bus.event.RemoteApplicationEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoginFailureEvent extends RemoteApplicationEvent {
  private static final long serialVersionUID = 2015746014404913527L;
  private UUID userId;

  public LoginFailureEvent() {
    super();
  }

  public LoginFailureEvent(Object source, String originService, UUID userId) {
    super(source, originService);
    this.userId = userId;
  }
}
