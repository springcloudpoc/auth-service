package com.jsm.auth.event;

import java.util.Date;
import java.util.UUID;
import org.springframework.cloud.bus.event.RemoteApplicationEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoginSuccessEvent extends RemoteApplicationEvent {
  private static final long serialVersionUID = 8545891005719487881L;
  private UUID userId;
  private Date date;

  public LoginSuccessEvent() {
    super();
  }

  public LoginSuccessEvent(Object source, String originService, UUID userId) {
    super(source, originService);
    this.userId = userId;
    this.date = new Date();
  }
}
