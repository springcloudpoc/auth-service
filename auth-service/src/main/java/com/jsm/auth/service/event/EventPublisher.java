package com.jsm.auth.service.event;

import org.springframework.hateoas.Link;

public interface EventPublisher {
  void publishLoginFailure(Link userLink);

  void publishLoginSuccess(Link userLink);
}
