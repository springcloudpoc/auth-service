package com.jsm.auth.service.event;

import java.util.UUID;
import org.springframework.context.ApplicationContext;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;
import com.jsm.auth.event.LoginFailureEvent;
import com.jsm.auth.event.LoginSuccessEvent;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@Data
@RequiredArgsConstructor
public class EventPublisherImpl implements EventPublisher {
  private final @NonNull ApplicationContext appContext;
  private final @NonNull IdLookup<UUID> idLookup;

  @Override
  public void publishLoginSuccess(Link userLink) {
    LoginSuccessEvent event =
        new LoginSuccessEvent(this, appContext.getId(), idLookup.getId(userLink));

    appContext.publishEvent(event);
  }

  @Override
  public void publishLoginFailure(Link userLink) {
    LoginFailureEvent event =
        new LoginFailureEvent(this, appContext.getId(), idLookup.getId(userLink));

    appContext.publishEvent(event);
  }

}
