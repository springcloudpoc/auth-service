package com.jsm.auth.service.event;

import org.springframework.hateoas.Link;

public interface IdLookup<T> {
  T getId(Link link);
}
