package com.jsm.auth.service.event;

import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

@Service
public class UuidLookup implements IdLookup<UUID> {
  final String regex =
      "\\/users\\/([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})$";
  final Pattern pattern = Pattern.compile(regex);

  @Override
  public UUID getId(Link link) {
    Matcher matcher = pattern.matcher(link.expand(new HashMap<String, Object>()).getHref());

    if (matcher.find()) {
      return UUID.fromString(matcher.group(1));
    }

    return null;
  }

}
