package com.jsm.auth.service.provider;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.hateoas.Resource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import com.jsm.auth.service.event.EventPublisher;
import com.jsm.users.client.UserService;
import com.jsm.users.client.domain.UserDTO;
import com.jsm.users.client.domain.UserValidateDTO;
import feign.FeignException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceAuthenticationProvider implements AuthenticationProvider {
  private final @NonNull UserService userService;
  private final @NonNull EventPublisher eventPublisher;

  private List<GrantedAuthority> getGrantedAuthorities(UserDTO user) {
    return user.getEntitlements().stream().map(e -> {
      return new SimpleGrantedAuthority("ROLE_" + e.getName());
    }).collect(Collectors.toList());
  }

  private String getAuthenticationCredentials(Object object) {
    return Optional.ofNullable(object)
        .orElseThrow(() -> new BadCredentialsException("Credentials must not be null")).toString();
  }

  private String getNonNullUsername(Object username) {
    return Optional.ofNullable(username)
        .orElseThrow(() -> new BadCredentialsException("Username must not be null")).toString();
  }

  private Authentication validate(Authentication authentication) {
    String name = this.getNonNullUsername(authentication.getPrincipal());
    String password = this.getAuthenticationCredentials(authentication.getCredentials());
    Optional<Resource<UserDTO>> userResource = Optional.empty();
    Authentication output = null;

    // validate user
    try {
      userResource =
          Optional.ofNullable(userService.validateUser(name, new UserValidateDTO(password)));

      if (name.equals(userResource
          .orElseThrow(() -> new BadCredentialsException("User service communication failure"))
          .getContent().getUsername())) {
        output = new UsernamePasswordAuthenticationToken(name, "N/A",
            getGrantedAuthorities(userResource.get().getContent()));
      }
    } catch (FeignException | BadCredentialsException ex) {
      log.debug("error fetching user", ex);

      if (userResource.isPresent()) {
        eventPublisher.publishLoginSuccess(userResource.get().getId());
      }

      throw new BadCredentialsException("Authentication failed", ex);
    }

    eventPublisher.publishLoginSuccess(userResource.get().getId());
    return output;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    return Optional.ofNullable(authentication).isPresent() ? this.validate(authentication) : null;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }

}
