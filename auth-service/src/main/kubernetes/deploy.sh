#!/bin/bash

files=$(kubectl get deployment auth-service)
if [[ $? != 0 ]]; then
	kubectl create -f ./auth-service-deployment.yaml
	kubectl expose auth-service --port=8083 --target-port=8083
else
	kubectl replace -f ./auth-service-deployment.yaml
fi
