<html>
<head>
<link rel="stylesheet" href="../css/wro.css"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
</head>
<body>
	<div class="container h-100">
	    <div class="row align-items-center h-100">
	        <div class="col-6 mx-auto">
				<div class="card">
					<div class="card-header">Please Confirm</div>
					<div class="card-body">
						<p>
							Do you authorize "${authorizationRequest.clientId}" to access your protected resources
							with scope ${authorizationRequest.scope?join(", ")}.
						</p>
						<div class="row">
						<div class="col-sm-6">
							<form id="confirmationForm" name="confirmationForm"
								action="../oauth/authorize" method="post">
								<input name="user_oauth_approval" value="true" type="hidden" />
					        	<input type="hidden" id="csrf_token" name="${_csrf.parameterName}" value="${_csrf.token}"/>
								<button class="btn btn-primary" type="submit">Approve</button>
							</form>
						</div>
						<div class="col-sm-6">
						<form id="denyForm" name="confirmationForm"
							action="../oauth/authorize" method="post">
							<input name="user_oauth_approval" value="false" type="hidden" />
				        	<input type="hidden" id="csrf_token" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<button class="btn btn-secondary" type="submit">Deny</button>
						</form>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="../js/wro.js"	type="text/javascript"></script>
</body>
</html>