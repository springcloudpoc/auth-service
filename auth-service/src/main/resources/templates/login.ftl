<html>
<head>
<link rel="stylesheet" href="css/wro.css"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
</head>
<body>
	<div class="container h-100">
	    <div class="row align-items-center h-100">
	        <div class="col-6 mx-auto">
				<div class="card">
					<div class="card-header">IDM Login</div>
					<div class="card-body">
						<form role="form" action="login" method="post">
						  <div class="form-group">
						    <label for="username">Email:</label>
						    <input type="text" class="form-control" id="username" name="username"/>
						  </div>
						  <div class="form-group">
						    <label for="password">Password:</label>
						    <input type="password" class="form-control" id="password" name="password"/>
						  </div>
						  <input type="hidden" id="csrf_token" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						  <button type="submit" class="btn btn-primary">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="js/wro.js" type="text/javascript"></script>
</body>
</html>
