package com.jsm.auth.service;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.jsm.users.client.UserService;

@Configuration
public class ItConfig {
  @Bean
  public UserService userService() {
    return Mockito.mock(UserService.class);
  }
}
