package com.jsm.auth.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsm.users.client.UserService;
import com.jsm.users.client.domain.UserDTO;
import com.jsm.users.client.domain.UserValidateDTO;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT,
    properties = {"disableFeign=true", "spring.cloud.bus.enabled=false"})
public class ItTest {
  @LocalServerPort
  private int port;

  @Autowired
  private UserService userService;
  @Autowired
  MockMvc mockMvc;
  @Autowired
  ObjectMapper objectMapper;

  final String regex = "(.*?)=(.*?)($|;|,(?! ))";
  final Pattern pattern = Pattern.compile(regex);
  private SessionHolder sessionHolder = null;

  @Test
  public void invalidLoginReturns302() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    List<BasicNameValuePair> map = new ArrayList<>();
    map.add(new BasicNameValuePair("email", "first.last@example.com"));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/login").with(csrf()).headers(headers)
            .content(EntityUtils.toString(new UrlEncodedFormEntity(map))))
        .andExpect(status().isFound()).andExpect(redirectedUrl("/login?error"));
  }

  private Resource<UserDTO> generateUserDTO(String username) {
    UserDTO user = new UserDTO();
    Resource<UserDTO> userResource = new Resource<UserDTO>(user);

    user.setUsername(username);
    userResource.add(new Link("http://localhost/users/123", "self"));

    return userResource;
  }

  private MultiValueMap<String, String> convertHeaders(MvcResult result) {
    MultiValueMap<String, String> output = new HttpHeaders();

    result.getResponse().getHeaderNames().stream().forEach(e -> {
      output.put(e, result.getResponse().getHeaders(e));
    });

    return output;
  }

  private <T> ResponseEntity<T> convert(MvcResult result, Class<T> clazz) {
    T body;
    try {
      body = clazz.cast(result.getResponse().getContentAsString());
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    return new ResponseEntity<>(body, convertHeaders(result),
        HttpStatus.valueOf(result.getResponse().getStatus()));
  }

  private ResponseEntity<String> login() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    List<BasicNameValuePair> map = new ArrayList<>();
    map.add(new BasicNameValuePair("username", "user"));
    map.add(new BasicNameValuePair("password", "pass"));

    // setup mock user service response
    Mockito.when(userService.validateUser("user", new UserValidateDTO("pass")))
        .thenReturn(this.generateUserDTO("user"));

    try {
      return this.convert(mockMvc
          .perform(MockMvcRequestBuilders.post("/login").headers(headers).with(csrf())
              .content(EntityUtils.toString(new UrlEncodedFormEntity(map))))
          .andDo(new ResultHandler() {
            @Override
            public void handle(MvcResult result) throws Exception {
              sessionHolder = new SessionHolder(result.getRequest().getSession());
            }
          }).andReturn(), String.class);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    // return this.restTemplate.postForEntity("http://localhost:" + port + "/login", request,
    // String.class);
  }

  @Test
  public void validateLogin() {
    ResponseEntity<String> response = this.login();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FOUND);
    assertThat(response.getHeaders().get(HttpHeaders.LOCATION)).isEqualTo(Arrays.asList("/"));
  }

  HttpHeaders createAuthHeaders(String username, String password) {
    return new HttpHeaders() {
      {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        set("Authorization", authHeader);
      }
    };
  }

  private void authGrantHeaders() {
    if (sessionHolder == null) {
      this.login();
    }
  }

  private ResultActions authorizeRedirect() throws Exception {
    UserDTO user = new UserDTO();
    user.setUsername("user");
    Resource<UserDTO> userResource = new Resource<UserDTO>(user);

    userResource.add(new Link("http://localhost/users/123", "self"));

    Mockito.when(userService.validateUser("user", new UserValidateDTO("pass")))
        .thenReturn(userResource);

    this.authGrantHeaders();

    UriComponentsBuilder builder = UriComponentsBuilder.fromPath("/oauth/authorize")
        .queryParam("client_id", "user-portal")
        .queryParam("redirect_uri", "http://example.com/login").queryParam("response_type", "code")
        .queryParam("scope", "all").queryParam("state", "stateId");

    return mockMvc.perform(MockMvcRequestBuilders.get(builder.build().encode().toUriString())
        .session(sessionHolder.getSession())).andExpect(status().isOk());
    // return this.restTemplate.exchange(builder.build().encode().toUriString(), HttpMethod.GET,
    // request, String.class);
  }

  private ResultActions getTokenCodeRedirect()
      throws ParseException, UnsupportedEncodingException, IOException, Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    List<BasicNameValuePair> map = new ArrayList<>();
    map.add(new BasicNameValuePair("user_oauth_approval", "true"));

    // setup authorization get
    this.authorizeRedirect();

    return mockMvc.perform(MockMvcRequestBuilders.post("/oauth/authorize").with(csrf())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED).session(sessionHolder.getSession())
        .content(EntityUtils.toString(new UrlEncodedFormEntity(map))));
  }

  @Test
  public void getConfirmAuthScreen() throws Exception {
    ResultActions response = this.authorizeRedirect();
    response.andExpect(status().isOk());
  }

  @Test
  public void getTokenCodeRedirectContainsCode() throws Exception {
    getTokenCodeRedirect().andExpect(status().isFound())
        .andExpect(redirectedUrlPattern("http://example.com/login?code=*&state=stateId"));
  }

  @Test
  public void getTokenCodeRedirectToken() throws Exception {
    ResultActions response = getTokenCodeRedirect();

    String redirectLocation = response.andReturn().getResponse().getHeader(HttpHeaders.LOCATION);
    List<NameValuePair> queryParams =
        URLEncodedUtils.parse(new URI(redirectLocation), Charset.defaultCharset());
    List<NameValuePair> codeList =
        queryParams.stream().filter(e -> e.getName().equals("code")).collect(Collectors.toList());

    HttpHeaders headers = this.createAuthHeaders("user-portal", "user-portal-secret");


    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
    map.add("client_id", "user-portal");
    map.add("client_secret", "user-portal-secret");
    map.add("code", codeList.get(0).getValue());
    map.add("redirect_uri", "http://example.com/login");
    map.add("grant_type", "authorization_code");

    mockMvc
        .perform(MockMvcRequestBuilders.post("/oauth/token").params(map)
            .contentType(MediaType.APPLICATION_JSON).headers(headers))
        .andExpect(status().isOk()).andExpect(jsonPath("$.access_token").exists())
        .andExpect(jsonPath("$.refresh_token").exists())
        .andExpect(jsonPath("$.scope").value("all"));
  }

  @Test
  public void getTokenPasswordGrant() throws Exception {
    HttpHeaders headers = this.createAuthHeaders("user-portal", "user-portal-secret");

    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
    map.add("username", "user");
    map.add("password", "pass");
    map.add("grant_type", "password");

    Mockito.when(userService.validateUser("user", new UserValidateDTO("pass")))
        .thenReturn(this.generateUserDTO("user"));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/oauth/token").params(map)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED).headers(headers))
        .andExpect(status().isOk()).andExpect(jsonPath("$.access_token").exists())
        .andExpect(jsonPath("$.refresh_token").exists())
        .andExpect(jsonPath("$.scope").value("all"));
  }

}
