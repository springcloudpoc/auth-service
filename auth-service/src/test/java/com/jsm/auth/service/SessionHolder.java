package com.jsm.auth.service;

import javax.servlet.http.HttpSession;
import org.springframework.mock.web.MockHttpSession;

public class SessionHolder {
  private SessionWrapper session;

  public SessionHolder(HttpSession session) {
    this.session = new SessionWrapper(session);
  }

  public SessionWrapper getSession() {
    return session;
  }

  public void setSession(SessionWrapper session) {
    this.session = session;
  }

  static class SessionWrapper extends MockHttpSession {
    private final HttpSession httpSession;

    public SessionWrapper(HttpSession httpSession) {
      this.httpSession = httpSession;
    }

    @Override
    public Object getAttribute(String name) {
      return this.httpSession.getAttribute(name);
    }

    @Override
    public void setAttribute(String name, Object value) {
      this.httpSession.setAttribute(name, value);
    }

  }
}
