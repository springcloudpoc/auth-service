package com.jsm.auth.service.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.HashMap;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import com.jsm.auth.service.event.EventPublisher;
import com.jsm.users.client.UserService;
import com.jsm.users.client.domain.UserDTO;
import com.jsm.users.client.domain.UserValidateDTO;
import feign.FeignException;
import feign.Response;
import lombok.RequiredArgsConstructor;

public class UserServiceAuthenticationProviderTest {
  UserServiceAuthenticationProvider underTest;
  UserService userService;
  EventPublisher eventPublisher;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void setup() {
    userService = Mockito.mock(UserService.class);
    eventPublisher = Mockito.mock(EventPublisher.class);
    underTest = new UserServiceAuthenticationProvider(userService, eventPublisher);
  }

  @Test
  public void usernamePasswordTokenSupported() {
    assertTrue(underTest.supports(UsernamePasswordAuthenticationToken.class));
  }

  @Test
  public void allowNullAuthentication() {
    Authentication actual = underTest.authenticate(null);

    assertNull(actual);
  }

  @Test
  public void nullCredentialsRaisesException() {
    thrown.expect(BadCredentialsException.class);
    thrown.expectMessage("Credentials must not be null");

    underTest.authenticate(new UsernamePasswordAuthenticationToken("user", null));
  }

  @Test
  public void nullUsernameRaisesException() {
    thrown.expect(BadCredentialsException.class);
    thrown.expectMessage("Username must not be null");

    underTest.authenticate(new UsernamePasswordAuthenticationToken(null, "pass"));
  }

  @Test
  public void nullUserServiceResponseRaisesException() {
    thrown.expect(BadCredentialsException.class);
    thrown.expectMessage("Authentication failed");
    thrown.expectCause(
        new InnerMatcher<BadCredentialsException>("User service communication failure"));

    underTest.authenticate(new UsernamePasswordAuthenticationToken("invalid", "pass"));
  }

  @Test
  public void feignExceptionRaisesBadCredentialsException() {
    thrown.expect(BadCredentialsException.class);
    thrown.expectMessage("Authentication failed");
    thrown.expectCause(new InnerMatcher<FeignException>("status 404 reading key"));

    Mockito.when(userService.validateUser("invalid", new UserValidateDTO("pass")))
        .thenThrow(FeignException.errorStatus("key", Response.builder()
            .status(HttpStatus.NOT_FOUND.value()).headers(new HashMap<>()).build()));

    underTest.authenticate(new UsernamePasswordAuthenticationToken("invalid", "pass"));
  }

  @Test
  public void validUserServiceResponseReturnsUserAuthentication() {
    UserDTO user = new UserDTO();
    user.setUsername("user");

    Mockito.when(userService.validateUser("user", new UserValidateDTO("pass")))
        .thenReturn(new Resource<UserDTO>(user));

    Authentication expected =
        new UsernamePasswordAuthenticationToken("user", "N/A", new ArrayList<>());
    Authentication actual =
        underTest.authenticate(new UsernamePasswordAuthenticationToken("user", "pass"));

    assertEquals(expected, actual);
  }

  @Test
  public void userServiceResponseUsernameNullReturnsNull() {
    Mockito.when(userService.validateUser("user", new UserValidateDTO("pass")))
        .thenReturn(new Resource<UserDTO>(new UserDTO()));

    Authentication actual =
        underTest.authenticate(new UsernamePasswordAuthenticationToken("user", "pass"));

    assertNull(actual);
  }

  @Test
  public void userServiceResponseDoesNotMatchReturnsNull() {
    UserDTO user = new UserDTO();
    user.setUsername("abc");

    Mockito.when(userService.validateUser("user", new UserValidateDTO("pass")))
        .thenReturn(new Resource<UserDTO>(user));

    Authentication actual =
        underTest.authenticate(new UsernamePasswordAuthenticationToken("user", "pass"));

    assertNull(actual);
  }

  @RequiredArgsConstructor
  class InnerMatcher<T extends Exception> extends BaseMatcher<T> {
    private final String innerMessage;

    @Override
    public boolean matches(Object item) {
      return innerMessage.equals(Exception.class.cast(item).getMessage());
    }

    @Override
    public void describeTo(Description description) {
      description.appendText(innerMessage);
    }

  }
}
